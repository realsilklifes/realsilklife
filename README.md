Our History
In 2000, we established a silk factory in Hangzhou, China, focusing
on high-quality silk wholesale, but at a low price.
In 2008 we set up a design studio dedicated to silk pajamas and
bedding designs. Our designers are from France, USA and Canada.
In 2010 we created the [realsilklife.com](https://realsilklife.com/) for the market in Europe
and North America. Thousands of customers are currently enjoying our
products.
Why Silk?
1. Because of its natural protein structure, silk is the most
hypoallergenic of all fabrics
2. An all-climate fabric, silk is warm and cozy in winter and
comfortably cool when temperatures rise. Its natural
temperature-regulating properties give silk this paradoxical
ability to cool and warm simultaneously. Silk garments thus
outperform other fabrics in both summer and winter. Silk worn as a
second layer warms without being bulky
3. Silk is highly absorbent and dries quickly. It can absorb up to 30%
of its weight in moisture without feeling damp. Silk will absorb
perspiration while letting your skin breathe
4. In spite of its delicate appearance, silk is relatively robust and
its smooth surface resists soil and odors well
5. While silk abrasion resistance is moderate, it is the strongest
natural fiber and, surprisingly, it easily competes with steel yarn
in tensile strength
6. Silk takes color well, washes easily, and is easy to work with in
spinning, weaving, knitting, and sewing
7. Silk mixes well with other animal and vegetable fibers.